# Usage:
# dumbo start word-count.py -input input.txt -output word-counts.txt
# dumbo cat word-counts.txt | sort -k2,2nr | head -n 100

# prepend line #:
# dumbo cat word-counts.txt | sort -k2,2nr | awk 'BEGIN { l=1 } { print l ": " $0; l=l+1 }' | head -n 500

import re
from nltk.corpus import stopwords
stops=set(stopwords.words('english'))

rexp = r'[a-zA-Z]*'

def mapFn(s):
  ss = s.strip()
  if not ss:
    return None
  m = re.match(rexp, ss)
  if m:
    return m.group(0).lower()
  else:
    return None

def filterFn(s):
  if not s or len(s) <= 1 or s in stops:
    return None
  return s

def mapper(key, value):
  l = filter(filterFn, map(mapFn, re.split(' |,|;|\.', value)))
  for s in l:
    yield s, 1

def reducer(key, values):
  yield key, sum(values)

if __name__ == "__main__":
  import dumbo
  dumbo.run(mapper, reducer, combiner=reducer)
