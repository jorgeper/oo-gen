# before this run:
# sed '/^ *You received this.*/d;/^ *To unsubscribe from.*/d;/^ *For more options, visit https.*/d;/^\s*$/d;/^ *-- *$/d;/^ *Sent: *$/d;/^Date:.*/d;/^To: .*/d;/^.*You received this message because.*/d' emails.txt > input.txt

import re
import md5
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import *

stemmer = PorterStemmer()
wnl = WordNetLemmatizer()

unusual_removed = 0
unusual_removed_lines = 0

english_vocab = set(w.lower() for w in nltk.corpus.words.words())
exceptions = ["msft", "nam", "fuck", "haha", "hahaha", "hahahaha", "hahahahaha", "fuuck"]
for ex in exceptions:
  english_vocab.add(ex)

ignore1 = r'^You received this.*$'
ignore2 = r'^To unsubscribe from.*'
ignore3 = r'^For more options, visit.*'
ignore4 = r'^--\s*$'
ignore5 = r'^You received this message because.*'
ignore6 = r'^.*Jorge Pereira.*'
ignore7 = r'^.*David Notario.*'
ignore8 = r'^.*Dennis Kiilerich.*'
ignore9 = r'^.*Ozan Ozhan.*'
ignore10 = r'^.*\d+ KB.*'

inline = r'^>+.*$'
cc = r'^CC:.*$'
to = r'^To:.*$'
fromOzan = r'^From:.*"?Ozan.*'
endThread = r'^---.*'
emptyLine = r'^$'
sentFrom = r'^Sent.*$'
importance = r'^Importance.*$'
date = r'^Date.*$'
subject = r'^Subject.*$'
replyStart = r'^On .* wrote:$'
replyStart2 = r'^From:.*$'
ozanReplyStart = r'^On .* Ozan Ozhan.*wrote:$'
windowsPhone = r'Sent from my Windows Phone.*'
attachments = r'^Attachments:.*$'
attachment = r'^.*\.jpg.*$'

inBlock = False
block = ''
digests = set()

with open('data/abaza_oo/input_prefiltered_15_years.txt') as f:
#with open('data/abaza_oo/input_test.txt') as f:
  for line in f:
    line = re.sub(r'https?:\/\/.*[\r\n]*', '', line, flags=re.MULTILINE)
    line = re.sub(r'ui=2\&view.*[\r\n]*', '', line, flags=re.MULTILINE)
    line = line.strip()

    if len(line) == 0 or \
        re.match(emptyLine, line) or \
        re.match(sentFrom, line) or \
        re.match(importance, line) or \
        re.match(date, line) or \
        re.match(windowsPhone, line) or \
        re.match(to, line, re.M|re.I) or \
        re.match(cc, line, re.M|re.I) or \
        re.match(inline, line) or \
        re.match(subject, line) or \
        re.match(attachments, line) or \
        re.match(attachment, line) or \
        re.match(ignore1, line) or \
        re.match(ignore2, line) or \
        re.match(ignore3, line) or \
        re.match(ignore4, line) or \
        re.match(ignore5, line) or \
        re.match(ignore6, line) or \
        re.match(ignore7, line) or \
        re.match(ignore8, line) or \
        re.match(ignore10, line):
      continue

    if not inBlock: # start a block
      if re.match(fromOzan, line, re.M|re.I) or \
          re.match(ozanReplyStart, line, re.M|re.I):
        inBlock = True
        m = md5.new()
    else: # end a block
      if re.match(endThread, line, re.M|re.I) or \
          re.match(replyStart, line, re.M|re.I) or \
          re.match(replyStart2, line, re.M|re.I):
        inBlock = False

        try:
          tokens = word_tokenize(block)
          tokens = [wnl.lemmatize(w) for w in tokens]

          text_vocab = set(w.lower() for w in tokens)
          if len(text_vocab) > 0:
            unusual = text_vocab.difference(english_vocab)
            percent_unusual = float(len(unusual)) / len(text_vocab)
            # DEBUG
            # print str(percent_unusual * 100) + ": " + "-".join(text_vocab) + ": " + block
            if percent_unusual >= 0.5:
              unusual_removed = unusual_removed + 1
            else:
              m.update(block)
              d = m.digest()
              if not (d in digests):
                digests.add(d)
                print block
        except: #UnicodeDecodeError:
          pass

        block = ''
      else:
        # Add line?
        try:
          tokens = word_tokenize(line)
          tokens = [wnl.lemmatize(w) for w in tokens]
          text_vocab = set(w.lower() for w in tokens)
          if len(text_vocab) > 3 and \
              (not re.match(r'^[0-9]+.*$', line)) and \
              (not re.match(r'\w+:.*$', line)):
            unusual = text_vocab.difference(english_vocab)
            percent_unusual = float(len(unusual)) / len(text_vocab)
            # DEBUG
            # print str(percent_unusual * 100) + ": " + "-".join(text_vocab) + ": " + line
            if percent_unusual >= 0.5:
              unusual_removed_lines = unusual_removed_lines + 1
            else:
              block = block + line + '\n'
        except: #UnicodeDecodeError:
          pass

print "removed blocks with unusual words: " + str(unusual_removed)
print "removed lines with unusual words: " + str(unusual_removed_lines)

